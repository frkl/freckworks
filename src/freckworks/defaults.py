# -*- coding: utf-8 -*-
import os
import sys

from appdirs import AppDirs
from bring.defaults import BRINGISTRY_PRELOAD_MODULES


freckworks_app_dirs = AppDirs("freckworks", "frkl")

if not hasattr(sys, "frozen"):
    FRECKWORKS_MODULE_BASE_FOLDER = os.path.dirname(__file__)
    """Marker to indicate the base folder for the `freckworks` module."""
else:
    FRECKWORKS_MODULE_BASE_FOLDER = os.path.join(
        sys._MEIPASS, "freckworks"  # type: ignore
    )  # type: ignore
    """Marker to indicate the base folder for the `freckworks` module."""

FRECKWORKS_RESOURCES_FOLDER = os.path.join(FRECKWORKS_MODULE_BASE_FOLDER, "resources")

FRECKWORKS_PRELOAD_MODULES = BRINGISTRY_PRELOAD_MODULES + [
    "freckworks.core.*",
    "freckworks.k8s.*",
    "freckworks.k8s.argocd.*",
    "freckworks.k8s.cert_manager.*",
    "freckworks.k8s.ingress_nginx.*",
    "freckworks.k8s.k3d.*",
    "freckworks.bring.*",
]

FRECKWORKS_CONFIG_FILE_NAME = "freckworks.yaml"
# FRECKWORKS_BRING_CONTEXT_FOLDER = os.path.join(FRECKWORKS_RESOURCES_FOLDER, "pkgs")

# FRECKWORKS_KUBERNETES_MANIFESTS = os.path.join(
#     freckworks_app_dirs.user_data_dir, "manifests"
# )

# PATH-related values
FRECKWORKS_BIN_FOLDER = os.path.join(freckworks_app_dirs.user_data_dir, "bin")
FRECKWORKS_PATH = [FRECKWORKS_BIN_FOLDER] + os.environ.get("PATH", os.defpath).split(
    os.pathsep
)

FRECKWORKS_BASE_TOPIC = "freckworks.tasks"
