# -*- coding: utf-8 -*-
from abc import abstractmethod
from typing import TYPE_CHECKING, Any, Iterable, Mapping, Optional

from freckworks.tasks import FreckTingTaskDesc
from tings.ting import SimpleTing


if TYPE_CHECKING:
    from freckworks.core.freckworks import FreckworksProject


class FreckTing(SimpleTing):
    def __init__(
        self,
        name: str,
        freckworks_project: "FreckworksProject",
        meta: Optional[Mapping[str, Any]],
    ):

        self._freckworks_project = freckworks_project
        self._task_desc = FreckTingTaskDesc(name=name)

        super().__init__(name=name, meta=meta)

    @property
    def task_desc(self) -> FreckTingTaskDesc:

        return self._task_desc

    def provides(self) -> Mapping[str, str]:

        return {}

    # def requires(self) -> Mapping[str, str]:
    #
    #     return {}
    #
    # async def retrieve(self, *value_names: str, **requirements) -> Mapping[str, Any]:
    #
    #     return {}

    @abstractmethod
    def upstream(self) -> Iterable[Mapping[str, Any]]:

        pass

    @abstractmethod
    async def resolve_values(
        self, *value_names: str, **requirements
    ) -> Mapping[str, Any]:
        pass

    async def retrieve(self, *value_names: str, **requirements) -> Mapping[str, Any]:

        self._task_desc.task_started()

        result = await self.resolve_values(*value_names, **requirements)

        self._task_desc.task_finished()

        return result
