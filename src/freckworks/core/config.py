# -*- coding: utf-8 -*-
import logging
import os
from typing import Any, Iterable, Mapping, Optional, Type, Union

from blessed import Terminal
from freckworks.core.freckworks import FreckworksProject
from frtls.async_helpers import wrap_async_task
from frtls.exceptions import FrklException
from frtls.formats.input_formats import SmartInput
from frtls.introspection.pkg_env import AppEnvironment
from tings.ting import SimpleTing
from tings.tingistry import Tingistry


log = logging.getLogger("freckworks")


class FreckworksProjectConfig(SimpleTing):
    def __init__(
        self,
        name: str,
        meta: Optional[Mapping[str, Any]] = None,
        terminal: Terminal = None,
    ):

        self._termminal: Optional[Terminal] = terminal
        self._tingistry_obj: Tingistry = meta["tingistry"]  # type: ignore
        super().__init__(name=name, meta=meta)

        self._project: Optional[FreckworksProject] = None

    def _invalidate(self) -> None:

        self._project = None

    def requires(self) -> Mapping[str, str]:

        return {
            "project_name": "string",
            "init_frecktings": "list",
            "init_values": "dict",
        }

    def provides(self) -> Mapping[str, str]:

        return {"project": "FreckworksProject"}

    async def _create_project(
        self,
        project_name: str,
        project_class: Union[str, Type] = FreckworksProject,
        init_frecktings: Optional[Iterable[Union[str, Mapping[str, Any]]]] = None,
        init_values: Optional[Mapping[str, Any]] = None,
        bring_contexts: Mapping[str, Mapping[str, Any]] = None,
    ) -> "FreckworksProject":

        if self._termminal is not None:
            terminal = self._termminal
        else:
            terminal = AppEnvironment().get_global("terminal")

        freckworks_project: FreckworksProject = self._tingistry_obj.create_singleting(  # type: ignore
            project_name,
            project_class,
            init_frecktings=init_frecktings,
            init_values=init_values,
            terminal=terminal,
        )  # type: ignore

        if bring_contexts:
            await freckworks_project._bring.add_extra_contexts(bring_contexts)

        return freckworks_project

    async def retrieve(self, *value_names: str, **requirements) -> Mapping[str, Any]:

        if self._project is None:

            self._project = await self.create_project(**requirements)

        return {"project": self._project}

    async def create_project(self, **requirements) -> FreckworksProject:

        init_frecktings = requirements["init_frecktings"]
        init_values = requirements["init_values"]
        project_name = requirements["project_name"]

        freckworks_project = await self._create_project(
            init_frecktings=init_frecktings,
            init_values=init_values,
            project_name=project_name,
        )

        return freckworks_project

    @property
    def project(self) -> FreckworksProject:

        if self._project is None:
            wrap_async_task(self.get_value, "project")
        return self._project  # type: ignore


class FolderFreckworksProjectConfig(FreckworksProjectConfig):
    def requires(self) -> Mapping[str, str]:

        return {"project_folder": "string"}

    async def create_project(self, **requirements) -> FreckworksProject:
        project_folder = requirements.get("project_folder", os.getcwd())

        project_config_file = os.path.join(project_folder, "freckworks.yaml")

        if not os.path.exists(project_config_file):
            raise FrklException(
                msg="Can't create freckworks project.",
                reason=f"Project config file does not exist: {project_config_file}",
            )

        si = SmartInput(project_config_file, content_type="yaml")
        project_config = await si.content_async()

        if "project_name" not in project_config.keys():
            project_config["project_name"] = os.path.basename(project_folder)

        bring_context_path = os.path.join(project_folder, "bring_pkgs")
        if os.path.isdir(bring_context_path):
            project_config["bring_contexts"] = {
                project_config["project_name"]: {
                    "type": "folder",
                    "indexes": [bring_context_path],
                }
            }

        project = await self._create_project(**project_config)
        return project
