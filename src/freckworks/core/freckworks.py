# -*- coding: utf-8 -*-

"""Main module."""
from typing import Any, Dict, Iterable, Mapping, MutableMapping, Optional, Type, Union

from blessed import Terminal
from bring.bring import Bring
from bring.pkg import PkgTing
from freckworks.core.frecktings import FreckTing
from freckworks.tasks import FreckworksProjectTaskDesc
from frtls.args.arg import Arg, RecordArg
from frtls.cli.terminal import create_terminal
from frtls.exceptions import FrklException
from frtls.tasks import TaskDesc
from frtls.types.typistry import TypistryPluginManager
from tings.ting import SimpleTing
from tings.tingistry import Tingistry
from tings.utils import process_metadata


class FreckTingCentral(object):
    def __init__(
        self,
        freckworks_project: "FreckworksProject",
        base_namespace: str,
        frecktings: Iterable[Mapping[str, Any]] = None,
    ):

        self._provides_map: Optional[MutableMapping[str, Mapping[str, Any]]] = None
        self._requires_map: Optional[MutableMapping[str, Mapping[str, Any]]] = None
        self._internal_var_map: Optional[MutableMapping[str, Mapping[str, Any]]] = None

        self._freckworks_project: FreckworksProject = freckworks_project
        self._tingistry: Tingistry = self._freckworks_project._tingistry_obj
        self._freck_ting_plugins: TypistryPluginManager = self._tingistry.typistry.get_plugin_manager(
            FreckTing, plugin_type="instance"
        )

        self._base_namespace = base_namespace
        self._proto_namespace = f"{self._base_namespace}.proto"
        self._freckting_namespace = f"{self._base_namespace}.frecktings"

        self._frecktings: Dict[str, FreckTing] = {}
        self._frecktings_processed: bool = False

        self._current_values: Dict[str, Any] = {}

        if frecktings:
            for f in frecktings:
                self.register_freckting(**f)

    def get_freckting(self, freckting_id: str) -> FreckTing:

        ft: Optional[FreckTing] = self._frecktings.get(freckting_id, None)

        if ft is None:
            raise FrklException(
                msg=f"Can't retrieve freckting '{freckting_id}'",
                reason="No freckting registered under that name.",
            )

        return ft

    def _invalidate(self) -> None:

        self._frecktings_processed = False
        self._provides_map = None
        self._requires_map = None
        self._internal_var_map = None

    def register_freckting(
        self,
        freckting_id: str,
        freckting_type: Optional[str] = None,
        input_map: Optional[Mapping[str, str]] = None,
        output_map: Optional[Mapping[str, str]] = None,
        **freckting_init: Any,
    ) -> None:

        self._invalidate()
        freckting_full_name = f"{self._freckting_namespace}.{freckting_id}"

        ft: Optional[FreckTing] = self._frecktings.get(freckting_id, None)
        if ft is not None:
            raise FrklException(
                msg=f"Can't register proto freckting with name '{freckting_id}'",
                reason="Name already registered.",
            )

        if not freckting_type:
            freckting_type = freckting_id

        freckting_cls: Optional[Type] = self._freck_ting_plugins.get_plugin(
            freckting_type
        )
        if freckting_cls is None:
            raise FrklException(
                msg=f"Can't create freckting with type: {freckting_type}",
                reason="Type not available.",
                solution=f"Available types: {', '.join(self._freck_ting_plugins.plugin_names)}",
            )

        freckting: FreckTing = self._tingistry.create_singleting(  # type: ignore
            name=freckting_full_name,
            ting_class=freckting_cls,
            ting_input=input_map,
            ting_output=output_map,
            prototing_namespace=self._proto_namespace,
            freckworks_project=self._freckworks_project,
            **freckting_init,
        )  # type: ignore

        self._frecktings[freckting_id] = freckting

        # reqs = []
        for up in freckting.upstream():

            if "freckting_id" not in up.keys():
                _up = dict(up)
                _up["freckting_id"] = _up["freckting_type"]
            else:
                _up = dict(up)

            self.register_freckting(**_up)
            # reqs.append(up_ting)

    @property
    def frecktings(self) -> Mapping[str, FreckTing]:

        if not self._frecktings_processed:
            self.requires_map  # noqa

        return self._frecktings

    @property
    def provides_map(self) -> Mapping[str, Mapping[str, Any]]:

        if self._provides_map is not None:
            return self._provides_map

        self._provides_map = {}
        for id, freckting in self._frecktings.items():

            out = freckting.output_map
            for k, v in out.items():
                # key = f"{id}.{k}"
                self._provides_map[k] = {"freckting": freckting, "arg_type": v}
                # dpath.new(result, key, v, separator=".")

        return self._provides_map

    @property
    def requires_map(self) -> Mapping[str, Mapping[str, Any]]:

        if self._requires_map is not None:
            return self._requires_map

        self._requires_map = {}
        self._internal_var_map = {}
        for id, freckting in self._frecktings.items():

            reqs = set()
            inp = freckting.input_map
            for k, v in inp.items():

                if k in self.provides_map.keys():
                    self._internal_var_map[k] = {"freckting": freckting, "arg_type": v}
                    reqs.add(self.provides_map[k]["freckting"])
                else:
                    self._requires_map[k] = {"freckting": freckting, "arg_type": v}

            freckting.set_requirements(*reqs)

        self._frecktings_processed = True
        return self._requires_map

    @property
    def internal_var_map(self) -> Mapping[str, Mapping[str, Any]]:

        if self._internal_var_map is None:
            self.requires_map  # noqa

        return self._internal_var_map  # type: ignore

    def set_value(self, key: str, value: Any):

        md = self.requires_map.get(key)
        if md is None:
            raise FrklException(
                msg=f"Can't set value for '{key}'.",
                reason="Key not a valid required value.",
            )

        freckting = md["freckting"]
        freckting.input.set_values(**{key: value})

        self._current_values[key] = value

    def set_values(self, **values):

        for k, v in values.items():
            self.set_value(k, v)

    def get_current_input(self) -> Mapping[str, Any]:

        return self._current_values

    def create_arg_map(self) -> Mapping[str, Arg]:

        args = {}
        for name, details in self.requires_map.items():

            arg = self._tingistry.arg_hive.get_arg(details["arg_type"])
            args[name] = arg

        return args


class FreckworksProject(SimpleTing):
    def __init__(
        self,
        name: str,
        meta: Optional[Mapping[str, Any]] = None,
        init_frecktings: Optional[Iterable[Union[str, Mapping[str, Any]]]] = None,
        init_values: Optional[Mapping[str, Any]] = None,
        terminal: Terminal = None,
    ):

        _md = process_metadata(name=name, meta=meta)

        if meta is None:
            raise Exception(
                "Can't create freckworks project: 'meta' argument not provided, this is a bug"
            )

        self._tingistry_obj: Tingistry = meta["tingistry"]

        self._bring: Bring = self._tingistry_obj.create_singleting(  # type: ignore
            "bring.mgmt", Bring
        )  # type: ignore

        self._freckting_mgmt = FreckTingCentral(
            freckworks_project=self, base_namespace=_md["full_name"]
        )

        super().__init__(name=name, meta=meta)

        self._default_pkg_context = "freckworks"
        if terminal is None:
            terminal = create_terminal()
        self._terminal: Terminal = terminal

        self._project_root_task = FreckworksProjectTaskDesc(self, name=name)

        if init_frecktings:
            for ft in init_frecktings:
                if isinstance(ft, str):
                    ft = {"freckting_id": ft}

                self.register_freckting(**ft)

        if init_values:
            for k, v in init_values.items():
                self.set_value(k, v)

    @property
    def project_root_task(self) -> FreckworksProjectTaskDesc:

        return self._project_root_task

    async def retrieve(self, *value_names: str, **requirements) -> Mapping[str, Any]:

        retr_map: Dict[FreckTing, Any] = {}
        for val in value_names:
            md = self._freckting_mgmt.provides_map.get(val)
            if md is None:
                raise Exception(
                    f"Can't retrieve '{val}', no freckting registered for this value."
                )
            freck = md["freckting"]
            retr_map.setdefault(freck, []).append(val)

        result: Dict[str, Any] = {}

        for freckting, val_names in retr_map.items():
            f_vals: Mapping[str, Any] = await freckting.get_values(  # type: ignore
                *val_names, resolve=True
            )  # type: ignore
            result.update(f_vals)

        # async def get_values_for_freckting(_freckting, _value_names):
        #     f_vals: Mapping[str, Any] = await _freckting.get_values(  # type: ignore
        #         *_value_names, resolve=True
        #     )  # type: ignore
        #     result.update(f_vals)
        #
        # async with create_task_group() as tg:
        #
        #     for freckting, val_names in retr_map.items():
        #         await tg.spawn(get_values_for_freckting, freckting, val_names)
        #         # f_vals: Mapping[str, Any] = await freckting.get_values(  # type: ignore
        #         #     *val_names, resolve=True
        #         # )  # type: ignore
        #         # result.update(f_vals)

        return result

    @property
    def terminal(self) -> Terminal:
        return self._terminal

    async def install(
        self,
        pkg_name: str,
        target: str,
        pkg_context: Optional[str] = None,
        parent_task_desc: TaskDesc = None,
        **vars: Any,
    ):

        if pkg_context is None:
            pkg_context = self._default_pkg_context

        ctx = self._bring.get_context(pkg_context)

        # pkgs = await ctx.get_pkgs()

        if ctx is None:
            raise FrklException(
                msg=f"Can't install pkg '{pkg_name}'.",
                reason=f"Context '{pkg_context}' not available.",
            )
        pkg: PkgTing = await ctx.get_pkg(pkg_name)

        target = await pkg.create_version_folder(
            vars=vars, target=target, parent_task_desc=parent_task_desc
        )

        return target

    def register_freckting(
        self,
        freckting_id: str,
        freckting_type: Optional[str] = None,
        input_map: Optional[Mapping[str, str]] = None,
        output_map: Optional[Mapping[str, str]] = None,
        **freckting_init,
    ) -> None:

        self._freckting_mgmt.register_freckting(
            freckting_id=freckting_id,
            freckting_type=freckting_type,
            input_map=input_map,
            output_map=output_map,
            **freckting_init,
        )
        self._invalidate_project()

    def _invalidate_project(self):

        self.invalidate()

    @property
    def frecktings(self) -> Mapping[str, FreckTing]:

        return self._freckting_mgmt.frecktings

    def requires(self) -> Mapping[str, str]:

        return {x: y["arg_type"] for x, y in self._freckting_mgmt.requires_map.items()}

    def provides(self) -> Mapping[str, str]:

        return {x: y["arg_type"] for x, y in self._freckting_mgmt.provides_map.items()}

    def set_value(self, key, value):

        self._freckting_mgmt.set_value(key, value)

    def set_values(self, **values: Any):

        self._freckting_mgmt.set_values(**values)

    def get_current_input(self):

        return self._freckting_mgmt.get_current_input()

    @property
    def project_info(self) -> Mapping[str, Any]:

        result: Dict[str, Any] = {}
        result["requires"] = self.requires()
        result["provides"] = self.provides()
        result["frecktings"] = list(self.frecktings.keys())
        result["values"] = self._freckting_mgmt._current_values
        # result["values"] = wrap_async_task(self.get_values)

        return result

    def calculate_args(self) -> RecordArg:

        arg_map = self._freckting_mgmt.create_arg_map()
        arg_obj = self._tingistry_obj.arg_hive.create_record_arg(arg_map)
        return arg_obj

    def get_args(self):

        return {"kube": {"arg_type": "any", "doc": "Kube python object"}}
