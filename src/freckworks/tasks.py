# -*- coding: utf-8 -*-
from typing import TYPE_CHECKING

from freckworks.defaults import FRECKWORKS_BASE_TOPIC
from frtls.tasks import TaskDesc


if TYPE_CHECKING:
    from freckworks.core.freckworks import FreckworksProject


class FreckworksProjectTaskDesc(TaskDesc):
    def __init__(self, freckworks_project: "FreckworksProject", **kwargs):
        self._project = freckworks_project
        kwargs["topic"] = FRECKWORKS_BASE_TOPIC
        super().__init__(**kwargs)


class FreckTingTaskDesc(TaskDesc):
    def __init__(self, **kwargs):
        kwargs["topic"] = FRECKWORKS_BASE_TOPIC
        super().__init__(**kwargs)


class FreckworksTaskDesc(TaskDesc):
    def __init__(self, **kwargs):
        kwargs["topic"] = FRECKWORKS_BASE_TOPIC
        super().__init__(**kwargs)
