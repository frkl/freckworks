# -*- coding: utf-8 -*-
import os
from typing import Optional

import asyncclick as click
from blessed import Terminal
from freckworks.core.freckworks import FreckworksProject
from freckworks.k8s.kube import Kube
from frtls.cli.group import FrklBaseCommand
from frtls.formats.output_formats import serialize


class FreckworksKubeCommandGroup(FrklBaseCommand):
    def __init__(
        self,
        project: FreckworksProject,
        name: str = None,
        terminal: Terminal = None,
        **kwargs,
    ):

        self._project: FreckworksProject = project
        self._kube: Optional[Kube] = None

        super(FreckworksKubeCommandGroup, self).__init__(
            name=name,
            invoke_without_command=True,
            no_args_is_help=True,
            chain=False,
            result_callback=None,
            terminal=terminal,
            **kwargs,
        )

    async def get_kube(self):

        if self._kube is None:
            self._kube = await self._project.get_value("kube")

        return self._kube

    async def _list_commands(self, ctx):

        result = ["info", "merge", "unmerge"]

        if "DEBUG" in os.environ.keys():
            result.append("dev")

        return result

    async def _get_command(self, ctx, name):

        command = None
        if name == "info":

            @click.command()
            @click.pass_context
            async def info(ctx):
                kube = await self.get_kube()
                info = await kube.get_info()
                info_str = serialize(info, format="yaml")
                click.echo(info_str)

            command = info

        elif name == "merge":

            @click.command()
            @click.argument("target_config", nargs=1, required=False)
            @click.option(
                "--set-current",
                "-c",
                help="set the current context to this configuration",
                is_flag=True,
                default=False,
            )
            @click.option(
                "--backup/--no-backup",
                help="whether to backup current config before merging",
                default=True,
            )
            @click.pass_context
            async def merge(ctx, target_config: str, set_current: bool, backup: bool):

                kube = await self.get_kube()
                await kube.merge_kubeconfig(
                    target_path=target_config,
                    set_current_context=set_current,
                    backup_orig_config=backup,
                )

            command = merge

        elif name == "unmerge":

            @click.command()
            @click.argument("target_config", nargs=1, required=False)
            @click.option(
                "new_context",
                "-c",
                help="the context to set as current after unmerging",
                required=False,
            )
            @click.option(
                "--backup/--no-backup",
                help="whether to backup current conrfig before unmerging",
                default=True,
            )
            @click.pass_context
            async def unmerge(ctx, target_config, new_context, backup):

                kube = await self.get_kube()
                await kube.unmerge_kubeconfig(
                    target_path=target_config,
                    new_current_context=new_context,
                    backup_orig_config=backup,
                )

            command = unmerge

        return command


# class KubeInfoCommand(Command):
#     def __init__(
#         self, name: str, kube: Kube, terminal: Terminal = None, **kwargs
#     ):
#
#         self._kube: Kube = kube
#
#         if terminal is None:
#             terminal = create_terminal()
#         self._terminal = terminal
#
#         super().__init__(name=name, callback=self.info, **kwargs)
#
#     async def info(self):
#
#         info = await self._kube.get_info()
#         info_str = serialize(info, format="yaml")
#         click.echo(info_str)
