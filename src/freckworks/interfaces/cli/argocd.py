# -*- coding: utf-8 -*-
import asyncclick as click
from freckworks.core.freckworks import FreckworksProject
from freckworks.k8s.argocd.argocd import ArgoCD
from frtls.cli.exceptions import handle_exc_async


@click.group()
@click.pass_context
@handle_exc_async
async def argocd(ctx):
    """freckops development utilities"""

    pass


@argocd.command()
@click.pass_context
@handle_exc_async
async def info(ctx):
    """Development enviornment management.

    Commands to help manage and provision a freckops development environment.
    """

    project: FreckworksProject = ctx.obj["project"]

    argocd: ArgoCD = await project.get_value("argocd")

    print(argocd)


@argocd.command()
@click.pass_context
@handle_exc_async
async def port_forward(ctx):

    project: FreckworksProject = ctx.obj["project"]

    argocd: ArgoCD = await project.get_value("argocd")

    pw = await argocd.get_default_admin_password()

    argocd_session = argocd.argo_client_session(username="admin", password=pw)

    click.echo(f"ArgoCD available on: http://localhost:{argocd_session.port}")
    click.echo("Default password: {}".format(pw))

    await argocd_session.run()
