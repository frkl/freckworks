# -*- coding: utf-8 -*-
import os

import click

from asyncclick import Command
from blessed import Terminal
from freckworks.core.freckworks import FreckworksProject
from frtls.cli.group import FrklBaseCommand
from frtls.cli.terminal import create_terminal
from frtls.formats.output_formats import serialize


COMMAND_GROUP_HELP = """'freckworks' is a declarative project management utility."""


class FreckworksProjectCommandGroup(FrklBaseCommand):
    def __init__(
        self,
        project: FreckworksProject,
        name: str = None,
        terminal: Terminal = None,
        **kwargs,
    ):

        self._project: FreckworksProject = project
        kwargs["help"] = COMMAND_GROUP_HELP

        super(FreckworksProjectCommandGroup, self).__init__(
            name=name,
            invoke_without_command=True,
            no_args_is_help=True,
            chain=False,
            result_callback=None,
            terminal=terminal,
            **kwargs,
        )

    async def _list_commands(self, ctx):

        result = ["info", "apply"]

        if "DEBUG" in os.environ.keys():
            result.append("dev")

        return result

    async def _get_command(self, ctx, name):

        command = None
        if name == "info":
            command = FreckworksProjectInfoCommand(
                name="info", project=self._project, terminal=self._terminal
            )
        elif name == "apply":
            command = FreckworksProjectApplyCommand(
                name="apply", project=self._project, terminal=self._terminal
            )

        return command


class FreckworksProjectInfoCommand(Command):
    def __init__(
        self, name: str, project: FreckworksProject, terminal: Terminal = None, **kwargs
    ):

        self._project: FreckworksProject = project

        if terminal is None:
            terminal = create_terminal()
        self._terminal = terminal

        super().__init__(name=name, callback=self.info, **kwargs)

    async def info(self):

        info_str = serialize(self._project.project_info, format="yaml")
        click.echo(info_str)


class FreckworksProjectApplyCommand(Command):
    def __init__(
        self, name: str, project: FreckworksProject, terminal: Terminal = None, **kwargs
    ):

        self._project: FreckworksProject = project

        if terminal is None:
            terminal = create_terminal()
        self._terminal = terminal

        args = self._project.calculate_args()
        params = args.to_cli_options()

        current_vars = self._project.get_current_input()
        _params = []
        for p in params:

            if p.name not in current_vars.keys():
                _params.append(p)

        super().__init__(name=name, callback=self.apply, params=_params, **kwargs)

    async def apply(self, **kwargs):

        self._project.set_values(**kwargs)

        values = await self._project.get_values()
        click.echo()
        import pp

        pp(values)
