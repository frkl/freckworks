# -*- coding: utf-8 -*-

import asyncclick as click
from freckworks.interfaces.cli.freckworks_command_group import FreckworksCommandGroup


click.anyio_backend = "asyncio"


cli = FreckworksCommandGroup()

# @click.command(
#     cls=FreckworksCommandGroup
# )
#
# @click.pass_context
# async def cli(ctx, task_log):
#
#     if not task_log:
#         task_log = ["tree"]
#
#     watchers: List[Union[str, Mapping[str, Any]]] = []
#     for to in task_log:
#         watchers.append(
#             {
#                 "type": to,
#                 "base_topics": [BRING_TASKS_BASE_TOPIC, FRECKWORKS_BASE_TOPIC],
#                 "terminal": terminal,
#             }
#         )
#
#     watch_mgmt = TaskWatchManager(typistry=bring_obj.typistry, watchers=watchers)
#
#     ctx.obj["watch_mgmt"] = watch_mgmt


# info_cmd = FreckworksProjectInfoCommand(
#     name="info", project=freckworks_project_config, terminal=terminal
# )
# cli.add_command(info_cmd)

# cli.add_command(project)
# cli.add_command(argocd)

if __name__ == "__main__":
    cli()
