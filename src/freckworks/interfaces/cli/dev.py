# -*- coding: utf-8 -*-
import grp

import asyncclick as click
from freckworks.core.freckworks import FreckworksProject
from freckworks.defaults import FRECKWORKS_PATH
from frtls.cli.exceptions import handle_exc_async
from frtls.formats.output_formats import serialize
from frtls.processes import command_exists


def check_dev_env():

    check = {}

    check["docker"] = command_exists("docker", extra_path=FRECKWORKS_PATH)

    import getpass

    username = getpass.getuser()
    docker_group = False
    for gr in grp.getgrall():
        gr_name = gr.gr_name
        if gr_name != "docker":
            continue
        mem = gr.gr_mem
        docker_group = username in mem
        break

    check["docker_group"] = docker_group

    check["k3d"] = command_exists("k3d", extra_path=FRECKWORKS_PATH)
    check["kubectl"] = command_exists("kubectl", extra_path=FRECKWORKS_PATH)
    check["helm"] = command_exists("helm", extra_path=FRECKWORKS_PATH)
    check["argo"] = command_exists("argo", extra_path=FRECKWORKS_PATH)

    return check


@click.group()
@click.option(
    "--cluster-name", "-n", help="the name of the k3d cluster", required=False
)
@click.pass_context
async def dev(ctx, cluster_name: str):
    """freckops development utilities"""

    pass


@dev.group()
@click.pass_context
async def env(ctx):
    """Development enviornment management.

    Commands to help manage and provision a freckops development environment.
    """

    pass


@env.command()
@click.pass_context
async def status(ctx):
    """Display information about development environment."""

    status = check_dev_env()
    click.echo(serialize(status, format="yaml"))


@env.command()
@click.pass_context
async def prepare(ctx):

    project: FreckworksProject = ctx.obj["project"]

    await project.prepare()


@dev.command()
@click.pass_context
@handle_exc_async
async def apply(ctx):
    """Create/initialize a new development project.

    This will create a freckops-compatible project structure, install required helper utilities, and create and
    provision a cluster to be used for development.
    """
    pass
