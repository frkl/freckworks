# -*- coding: utf-8 -*-
from freckworks.defaults import FRECKWORKS_PRELOAD_MODULES
from frtls.types.utils import load_modules


app_name = "freckworks"

_hi = load_modules(FRECKWORKS_PRELOAD_MODULES)  # type: ignore
pyinstaller = {"hiddenimports": [x.__name__ for x in _hi]}
