# -*- coding: utf-8 -*-
from typing import Any, Iterable, Mapping

from bring.context import BringContextTing
from bring.pkg import PkgTing
from freckworks.core.frecktings import FreckTing
from freckworks.core.freckworks import FreckworksProject


class BringPkg(FreckTing):

    _plugin_name = "bring_pkg"

    def __init__(
        self,
        name: str,
        freckworks_project: FreckworksProject,
        pkg_name: str,
        # target: str,
        pkg_context: str,
        meta: Mapping[str, Any] = None,
    ):

        self._pkg_name = pkg_name
        # self._target = target
        self._pkg_context = pkg_context

        super().__init__(name=name, freckworks_project=freckworks_project, meta=meta)

    async def resolve_values(
        self, *value_names: str, **requirements
    ) -> Mapping[str, Any]:

        bring = self._freckworks_project._bring

        context: BringContextTing = bring.get_context(self._pkg_context)  # type: ignore
        pkg: PkgTing = await context.get_pkg(self._pkg_name)

        target = requirements[f"{self._pkg_name}_pkg_target"]

        target = await pkg.create_version_folder(
            target=target, parent_task_desc=self.task_desc
        )

        return {f"{self._pkg_name}_pkg_path": target}

    def provides(self) -> Mapping[str, str]:

        return {f"{self._pkg_name}_pkg_path": "string"}

    def upstream(self) -> Iterable[Mapping[str, Any]]:

        return []

    def requires(self):
        return {f"{self._pkg_name}_pkg_target": f"{self._pkg_name}_pkg_target"}

    def get_args(self):

        return {
            f"{self._pkg_name}_pkg_target": {
                "arg_type": "string",
                "required": True,
                "doc": f"path to install {self._pkg_name} package into",
            }
        }
