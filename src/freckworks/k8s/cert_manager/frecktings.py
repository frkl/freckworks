# -*- coding: utf-8 -*-
from typing import Any, Iterable, Mapping

from freckworks.core.frecktings import FreckTing
from freckworks.k8s.cert_manager.cert_manager import CertManager


class CertManagerApp(FreckTing):

    _plugin_name = "cert_manager"

    async def resolve_values(
        self, *value_names: str, **requirements
    ) -> Mapping[str, Any]:

        kube = requirements["kube"]
        manifest_folder = requirements.get("cert_manager_manifest_folder", None)
        # namespace = requirements.get("cert_manager_namespace", "cert-manager")

        cert_manager = CertManager(
            kube=kube,
            bring=self._freckworks_project._bring,
            manifest_folder=manifest_folder,
        )

        await cert_manager.install(wait=False, parent_task_desc=self._task_desc)

        return {"cert_manager": cert_manager}

    def requires(self) -> Mapping[str, str]:

        return {
            "kube": "Kube",
            "cert_manager_manifest_folder": "cert_manager_manifest_folder",
            # "cert_manager_namespace": "cert_manager_namespace",
        }

    def provides(self) -> Mapping[str, str]:

        return {"cert_manager": "CertManager"}

    def upstream(self) -> Iterable[Mapping[str, Any]]:

        return []

    def get_args(self) -> Mapping[str, Mapping]:

        return {
            "cert_manager_manifest_folder": {
                "arg_type": "string",
                "doc": "folder for (cert-manager-) manifests",
                "required": False,
            },
            # "cert_manager_namespace": {
            #     "arg_type": "string",
            #     "required": False,
            #     "doc": "namespace to install cert-manager into",
            #     "default": "cert-manager",
            # },
        }
