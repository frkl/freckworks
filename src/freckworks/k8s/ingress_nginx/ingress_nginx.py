# -*- coding: utf-8 -*-
from pathlib import Path
from typing import Optional, Union

from bring.bring import Bring
from freckworks.k8s.kube import Kube
from freckworks.k8s.kube_app import KubeApp


class IngressNginx(KubeApp):
    def __init__(
        self,
        kube: Kube,
        bring: Bring,
        manifest_folder: Optional[Union[str, Path]] = None,
    ) -> None:

        app_installed_indicator = [
            "-n",
            "ingress-nginx",
            "--for=condition=Available",
            "--timeout=400s",
            "deployment/nginx-ingress-controller",
        ]
        bring_pkgs = [
            {
                "name": "ingress-nginx-augmented",
                "context": "retailiate",
                "vars": {"version": "nginx-0.30.0"},
            }
        ]

        super().__init__(
            app_name="ingress_nginx",
            kube=kube,
            bring=bring,
            manifest_folder=manifest_folder,
            bring_pks=bring_pkgs,
            namespace=None,
            app_installed_indicator=app_installed_indicator,
        )
