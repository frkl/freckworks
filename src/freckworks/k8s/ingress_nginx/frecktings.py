# -*- coding: utf-8 -*-
from typing import Any, Iterable, Mapping

from freckworks.core.frecktings import FreckTing
from freckworks.k8s.ingress_nginx.ingress_nginx import IngressNginx


class IngressNginxApp(FreckTing):

    _plugin_name = "ingress_nginx"

    async def resolve_values(
        self, *value_names: str, **requirements
    ) -> Mapping[str, Any]:

        kube = requirements["kube"]
        manifest_folder = requirements.get("ingress_nginx_manifest_folder", None)
        # namespace = requirements.get("cert_manager_namespace", "cert-manager")

        ingress_nginx = IngressNginx(
            kube=kube,
            bring=self._freckworks_project._bring,
            manifest_folder=manifest_folder,
        )

        await ingress_nginx.install(wait=False, parent_task_desc=self._task_desc)

        return {"ingress_nginx": ingress_nginx}

    def requires(self) -> Mapping[str, str]:

        return {
            "kube": "Kube",
            "ingress_nginx_manifest_folder": "ingress_nginx_manifest_folder",
            # "ingress_nginx_namespace": "ingress_nginx_namespace",
        }

    def provides(self) -> Mapping[str, str]:

        return {"ingress_nginx": "IngressNginx"}

    def upstream(self) -> Iterable[Mapping[str, Any]]:

        return []

    def get_args(self) -> Mapping[str, Mapping]:

        return {
            "ingress_nginx_manifest_folder": {
                "arg_type": "string",
                "doc": "folder for (ingress-nginx-) manifests",
                "required": False,
            },
            # "ingress_nginx_namespace": {
            #     "arg_type": "string",
            #     "required": False,
            #     "doc": "namespace to install ingress-nginx into",
            #     "default": "cert-manager",
            # },
        }
