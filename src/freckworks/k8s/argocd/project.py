# -*- coding: utf-8 -*-
from typing import Any, Iterable, Mapping, Optional, Union

from blessed import Terminal
from freckworks.core.freckworks import FreckworksProject
from tings.utils import process_metadata


class ArgoCDProject(FreckworksProject):
    def __init__(
        self,
        name: str,
        meta: Optional[Mapping[str, Any]] = None,
        task_watchers: Iterable[Union[str, Mapping[str, Any]]] = None,
        terminal: Terminal = None,
    ):

        _md = process_metadata(name=name, meta=meta)

        init_frecktings = ["kube", "argocd"]
        init_values = {"cluster_name": _md["name"]}
        super().__init__(
            name=name,
            meta=meta,
            init_frecktings=init_frecktings,
            init_values=init_values,
            terminal=terminal,
        )
