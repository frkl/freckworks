# -*- coding: utf-8 -*-
from typing import Any, Iterable, Mapping

from freckworks.core.frecktings import FreckTing
from freckworks.k8s.argocd.argocd import ArgoCD


class ArgoCDApp(FreckTing):

    _plugin_name = "argocd"

    async def resolve_values(
        self, *value_names: str, **requirements
    ) -> Mapping[str, Any]:

        kube = requirements["kube"]
        manifest_folder = requirements.get("argocd_manifest_folder", None)
        namespace = requirements.get("argocd_namespace", "argocd")

        argocd = ArgoCD(
            kube=kube,
            bring=self._freckworks_project._bring,
            manifest_folder=manifest_folder,
            namespace=namespace,
        )

        await argocd.install(wait=False, parent_task_desc=self._task_desc)

        return {"argocd": argocd}

    def requires(self) -> Mapping[str, str]:

        return {
            "kube": "Kube",
            "argocd_manifest_folder": "argocd_manifest_folder",
            "argocd_namespace": "argocd_namespace",
        }

    def provides(self) -> Mapping[str, str]:

        return {"argocd": "ArgoCD"}

    def upstream(self) -> Iterable[Mapping[str, Any]]:

        return []

    def get_args(self) -> Mapping[str, Mapping]:

        return {
            "argocd_manifest_folder": {
                "arg_type": "string",
                "doc": "folder for (argocd-) manifests",
                "required": False,
            },
            "argocd_namespace": {
                "arg_type": "string",
                "required": False,
                "doc": "namespace to install argocd into",
                "default": "argocd",
            },
        }
