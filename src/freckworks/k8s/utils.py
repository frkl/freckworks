# -*- coding: utf-8 -*-
from typing import Any, MutableMapping, Optional

import anyio
from frtls.formats.input_formats import auto_parse_dict_string
from kubernetes_asyncio.config.kube_config import KUBE_CONFIG_DEFAULT_LOCATION


async def read_kubecfg_file(
    kubecfg_path: Optional[str] = None
) -> MutableMapping[str, Any]:

    if not kubecfg_path:
        _kubecfg_path: str = KUBE_CONFIG_DEFAULT_LOCATION
    else:
        _kubecfg_path = kubecfg_path

    async with await anyio.aopen(_kubecfg_path) as f:
        target_content = await f.read()

    target_config: MutableMapping[str, Any] = auto_parse_dict_string(  # type: ignore
        target_content, content_type="yaml"
    )  # type: ignore

    return target_config


async def get_current_context(kubecfg_path: Optional[str]) -> str:

    target_config = await read_kubecfg_file(kubecfg_path=kubecfg_path)

    return target_config["current-context"]
