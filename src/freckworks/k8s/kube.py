# -*- coding: utf-8 -*-
import atexit
import logging
import os
import shutil
import tempfile
import time
from typing import Any, Dict, List, Mapping, MutableMapping, Optional, Union

import anyio
import kubernetes_asyncio
from freckworks.defaults import FRECKWORKS_PATH
from freckworks.k8s.utils import get_current_context, read_kubecfg_file
from frtls.exceptions import FrklException
from frtls.formats.input_formats import auto_parse_dict_string
from frtls.formats.output_formats import serialize
from frtls.introspection.pkg_env import AppEnvironment
from frtls.processes import get_command_path
from frtls.subprocesses import AsyncSubprocess
from kubernetes_asyncio.client import ApiClient, AppsV1Api, CoreV1Api, V1Namespace
from kubernetes_asyncio.config.kube_config import KUBE_CONFIG_DEFAULT_LOCATION
from ruamel.yaml import YAML


log = logging.getLogger("freckworks")
yaml = YAML()
yaml.default_flow_style = False


class KubeCtlException(Exception):
    def __init__(self, command, args, retcode, stdout, stderr):

        msg = f"Error executing kubectl command '{command}': {retcode} - {stdout}/{stderr}"
        super(KubeCtlException, self).__init__(msg)

        self._msg = msg
        self._command = command
        self._args = args
        self._retcode = retcode
        self._stdout = stdout
        self._stderr = stderr


class KubeCtl(AsyncSubprocess):
    def __init__(
        self,
        kube: "Kube",
        kubectl_command: str,
        *args,
        working_dir: Optional[str] = None,
        expected_ret_code=0,
        kubectl_path: Optional[str] = None,
        **env_vars,
    ):

        self._kube = kube
        cmd_args = [kubectl_command] + list(args)

        extra_path = None
        if kubectl_path:

            if os.path.isdir(kubectl_path):
                kubectl_path = os.path.join(kubectl_path, "kubectl")

            if not os.path.isfile(kubectl_path):
                raise FrklException(
                    msg=f"Can't execute kubectl command '{kubectl_command}'.",
                    reason=f"No kubectl executable found at: {kubectl_path}",
                )

            extra_path = os.path.dirname(kubectl_path)

        super().__init__(
            "kubectl",
            *cmd_args,
            working_dir=working_dir,
            expected_ret_code=expected_ret_code,
            extra_path=extra_path,
            **env_vars,
        )


class Kube(object):
    @classmethod
    async def create_obj(
        cls,
        kubecfg_path: Optional[str] = None,
        context_name: Optional[str] = None,
        kubectl_path: Optional[str] = None,
    ):

        self = Kube(
            kubecfg_path=kubecfg_path,
            context_name=context_name,
            kubectl_path=kubectl_path,
        )
        await self._load()
        return self

    def __init__(
        self,
        kubecfg_path: Optional[str] = None,
        context_name: Optional[str] = None,
        kubectl_path: Optional[str] = None,
    ):

        if not kubectl_path:
            app_details: AppEnvironment = AppEnvironment()
            app_dirs = app_details.get_app_dirs()
            if app_dirs is None:
                extra_path = FRECKWORKS_PATH
            else:
                extra_path = [
                    os.path.join(app_dirs.user_data_dir, "bin")
                ] + FRECKWORKS_PATH
            kubectl_path = get_command_path("kubectl", extra_path=extra_path)
        self._kubectl_path = kubectl_path

        if kubecfg_path is None:
            kubecfg_path = KUBE_CONFIG_DEFAULT_LOCATION
        kubecfg_path = os.path.realpath(os.path.expanduser(kubecfg_path))
        self._kubecfg_parent = os.path.dirname(kubecfg_path)
        self._kubecfg_path = kubecfg_path

        self._context_name = context_name

        self._kubeconfig_dict: Optional[Mapping[str, Any]] = None
        self._kubecfg_private: Optional[str] = None
        self._kube_config = None
        self._api_client = None
        self._core_api = None
        self._apps_api = None

        atexit.register(self._finalize)

    async def get_info(self):

        return {
            "info": {
                "kubecfg": self._kubecfg_path,
                "context": self._context_name,
                "namespaces": await self.namespaces,
            }
        }

    def _finalize(self):

        if self._kubecfg_private and os.path.exists(self._kubecfg_private):
            os.unlink(self._kubecfg_private)

    async def _load(self):

        self._kube_config = kubernetes_asyncio.client.Configuration()

        async with await anyio.aopen(self._kubecfg_path) as f:
            contents = await f.read()

        kubeconfig = auto_parse_dict_string(contents, content_type="yaml")

        if self._context_name is not None:
            kubeconfig["current-context"] = self._context_name
        else:
            self._context_name = kubeconfig.get("current-context")
            if not self._context_name:
                self._context_name = "default"
                kubeconfig["current-context"] = self._context_name

        temp = tempfile.NamedTemporaryFile(mode="w", delete=False)
        self._kubecfg_private = temp.name

        yaml.dump(kubeconfig, temp)
        temp.close()

        loader = kubernetes_asyncio.config.kube_config.KubeConfigLoader(kubeconfig)
        await loader.load_and_set(self._kube_config)
        os.environ["KUBECONFIG"] = self._kubecfg_private
        self._kubeconfig_dict = kubeconfig

    async def merge_kubeconfig(
        self,
        target_path: Optional[str] = None,
        set_current_context: bool = False,
        backup_orig_config: bool = True,
    ) -> None:

        if target_path is None:
            _target_path = KUBE_CONFIG_DEFAULT_LOCATION
        else:
            _target_path = target_path

        _target_path = os.path.realpath(os.path.expanduser(_target_path))

        if _target_path == self._kubecfg_path:
            log.debug("Merge target path is equal to kubeconfig path, doing nothing...")
            return

        current_context = None
        if not set_current_context:
            current_context = await get_current_context(_target_path)

        config_list = [self._kubecfg_path]
        if os.path.exists(_target_path):
            config_list.append(_target_path)

        config_merge = KubeCtl(
            self,
            "config",
            "view",
            "--merge",
            "--flatten",
            KUBECONFIG=":".join(config_list),
            kubectl_path=self._kubectl_path,
        )

        await config_merge.run(wait=True, raise_exception=True)

        stdout = await config_merge.stdout
        if backup_orig_config:
            timestamp = time.strftime("%Y%m%d-%H%M%S")
            backup_name = f"{_target_path}.{timestamp}.bak"

            shutil.copy2(_target_path, backup_name)

        stdout_dict = auto_parse_dict_string(stdout, content_type="yaml")
        if set_current_context:
            stdout_dict["current-context"] = self._context_name  # type: ignore
        else:
            stdout_dict["current-context"] = current_context  # type: ignore

        stdout = serialize(stdout_dict, format="yaml")

        with open(_target_path, "w") as f:
            f.write(stdout)

    async def unmerge_kubeconfig(
        self,
        target_path: Optional[str] = None,
        new_current_context: Optional[str] = None,
        backup_orig_config: bool = True,
    ):

        if target_path is None:
            _target_path = KUBE_CONFIG_DEFAULT_LOCATION
        else:
            _target_path = target_path

        _target_path = os.path.realpath(os.path.expanduser(_target_path))

        if self._kubeconfig_dict is None:
            raise Exception("Can't unmerge kubeconfig: can't find config")

        del_clusters = set()
        for cluster in self._kubeconfig_dict["clusters"]:
            del_clusters.add(cluster["name"])
        del_contexts = set()
        for context in self._kubeconfig_dict["contexts"]:
            del_contexts.add(context["name"])

        del_users = set()
        for user in self._kubeconfig_dict["users"]:
            del_users.add(user["name"])

        target_config: MutableMapping[str, Any] = await read_kubecfg_file(_target_path)

        target_config_new_clusters = []

        for cluster in target_config["clusters"]:
            if cluster["name"] not in del_clusters:
                target_config_new_clusters.append(cluster)
        target_config["clusters"] = target_config_new_clusters

        current_context = target_config["current-context"]

        target_config_new_contexts = []
        _new_current_context = new_current_context
        for context in target_config["contexts"]:
            if context["name"] not in del_contexts:
                if not _new_current_context:
                    _new_current_context = context["name"]
                target_config_new_contexts.append(context)
        target_config["contexts"] = target_config_new_contexts

        if current_context in del_contexts or new_current_context:
            if new_current_context:
                target_config["current-context"] = new_current_context
            elif _new_current_context:
                target_config["current-context"] = _new_current_context
            else:
                target_config.pop("current-context", None)

        target_config_new_users = []
        for user in target_config["users"]:
            if user["name"] not in del_users:
                target_config_new_users.append(user)
        target_config["users"] = target_config_new_users

        if backup_orig_config:
            timestamp = time.strftime("%Y%m%d-%H%M%S")
            backup_name = f"{_target_path}.{timestamp}.bak"

            shutil.copy2(_target_path, backup_name)

        serialize(
            target_config,
            format="yaml",
            target={"target": _target_path, "target_opts": {"force": True}},
        )

    @property
    def api_client(self) -> ApiClient:

        if self._api_client is not None:
            return self._api_client

        self._api_client = ApiClient(self._kube_config)
        return self._api_client

    @property
    def apps_api(self) -> AppsV1Api:

        if self._apps_api is not None:
            return self._apps_api

        self._apps_api = AppsV1Api(self._api_client)
        return self._apps_api

    @property
    def core_api(self) -> CoreV1Api:

        if self._core_api is not None:
            return self._core_api

        self._core_api = CoreV1Api(self.api_client)
        return self._core_api

    @property
    async def namespaces(self) -> List[str]:

        namespaces = await self.core_api.list_namespace()

        result = []
        for ns in namespaces.items:
            result.append(ns.metadata.name)
        return result

    async def ensure_namespace(self, name: str):

        if name in await self.namespaces:
            return

        ns_obj = V1Namespace(metadata={"name": name})
        await self.core_api.create_namespace(ns_obj)

    async def ctl(
        self, command: str, *args, wait=True, raise_exception=True, no_run=False
    ) -> KubeCtl:

        ctl = KubeCtl(self, command, *args, kubectl_path=self._kubectl_path)

        if not no_run:
            await ctl.run(wait=wait, raise_exception=raise_exception)
        return ctl

    def ctl_context(self, command: str, *args, **kwargs) -> KubeCtl:

        ctl = KubeCtl(self, command, *args, kubectl_path=self._kubectl_path, **kwargs)
        return ctl

    def port_forward(self, *args, **kwargs) -> KubeCtl:

        return self.ctl_context("port-forward", *args, **kwargs)

    async def apply(self, data: Union[Dict, str], wait=True, raise_exception=True):

        if isinstance(data, str):
            ctl = await self.ctl(
                "apply", "-f", data, wait=wait, raise_exception=raise_exception
            )
            return ctl

        else:

            with tempfile.NamedTemporaryFile(mode="w") as temp:

                yaml.dump(data, temp)
                temp.flush()
                ctl = await self.ctl(
                    "apply", "-f", temp.name, wait=wait, raise_exception=raise_exception
                )
                return ctl

    async def create(self, data: Union[Dict, str], wait=True, raise_exception=True):

        if isinstance(data, str):
            ctl = await self.ctl(
                "create", "-f", data, wait=wait, raise_exception=raise_exception
            )
            return ctl

        else:
            with tempfile.NamedTemporaryFile(mode="w") as temp:

                yaml.dump(data, temp)
                temp.flush()
                ctl = await self.ctl(
                    "create",
                    "-f",
                    temp.name,
                    wait=wait,
                    raise_exception=raise_exception,
                )
                return ctl

    async def delete(self, data: Union[Dict, str], wait=True, raise_exception=True):

        if isinstance(data, str):
            ctl = await self.ctl(
                "delete", "-f", data, wait=wait, raise_exception=raise_exception
            )
            return ctl

        else:
            with tempfile.NamedTemporaryFile(mode="w") as temp:

                yaml.dump(data, temp)
                temp.flush()
                ctl = await self.ctl(
                    "delete",
                    "-f",
                    temp.name,
                    wait=wait,
                    raise_exception=raise_exception,
                )
                return ctl

    # async def wait(
    #     self, condition: str, labels: Dict = None, timeout=300, raise_exception=True
    # ):
    #
    #     cmd_list = [
    #         "wait",
    #         "-n",
    #         "ikh-mgmt",
    #         f"--for={condition}",
    #         "pod",
    #         f"--timeout={timeout}s",
    #     ]
    #
    #     if labels:
    #
    #         labels_list = []
    #         for k, v in labels.items():
    #             labels_list.append("-l")
    #             labels_list.append(f"{k}={v}")
    #
    #         cmd_list.extend(labels_list)
    #
    #     ctl = await self.ctl(*cmd_list, raise_exception=raise_exception)
    #
    #     return ctl
