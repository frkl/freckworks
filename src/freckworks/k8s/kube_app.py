# -*- coding: utf-8 -*-
import atexit
import os
import shutil
import tempfile
from pathlib import Path
from typing import Any, Iterable, List, Mapping, Optional, Union

import anyio
from bring.bring import Bring
from bring.context import BringContextTing
from freckworks.k8s.kube import Kube
from freckworks.tasks import FreckworksTaskDesc
from frtls.tasks import TaskDesc


class KubeApp(object):

    default_wait_timeout = 300
    default_wait_recheck_interval = 2

    def __init__(
        self,
        app_name: str,
        kube: Kube,
        bring: Bring,
        bring_pks: Iterable[Mapping[str, Any]],
        manifest_folder: Optional[Union[Path, str]],
        namespace: Optional[str] = None,
        app_installed_indicator: Optional[Iterable[str]] = None,
    ):

        self._app_name = app_name
        self._namespace: Optional[str] = namespace

        self._kube = kube
        self._bring = bring
        self._bring_pkgs: Iterable[Mapping[str, Any]] = bring_pks
        self._app_installed_indicator: Optional[Iterable[str]] = app_installed_indicator

        if manifest_folder is None:
            tempdir = tempfile.mkdtemp()

            def delete_temp_dir():
                shutil.rmtree(tempdir, ignore_errors=True)

            atexit.register(delete_temp_dir)
            _manifest_folder: str = tempdir
        elif isinstance(manifest_folder, Path):
            _manifest_folder = manifest_folder.resolve().as_posix()
        else:
            _manifest_folder = os.path.expanduser(manifest_folder)

        self._manifest_folder: str = _manifest_folder

    @property
    def namespace(self) -> Optional[str]:
        return self._namespace

    @property
    def app_name(self) -> str:

        return self._app_name

    async def ensure_manifests(self, parent_task_desc: TaskDesc = None):

        target = {"target": self._manifest_folder, "merge_strategy": "overwrite"}

        for bring_pkg in self._bring_pkgs:

            pkg_name = bring_pkg["name"]
            pkg_context = bring_pkg["context"]
            pkg_vars = bring_pkg.get("vars", {})

            _ctx: BringContextTing = self._bring.get_context(
                pkg_context
            )  # type: ignore
            pkg = await _ctx.get_pkg(pkg_name)

            import pp

            pp(pkg)

            await pkg.create_version_folder(
                vars=pkg_vars, target=target, parent_task_desc=parent_task_desc
            )

    async def plugin_ready(self, parent_task_desc: Optional[TaskDesc] = None) -> bool:

        return await self.kube_app_ready(wait=False, parent_task_desc=parent_task_desc)

    async def kube_app_ready(
        self, wait: bool = False, parent_task_desc: Optional[TaskDesc] = None
    ) -> bool:

        if wait and self._app_installed_indicator is not None:
            # timeout = f"{KubeApp.default_wait_timeout}s"
            td_wait: Optional[FreckworksTaskDesc] = FreckworksTaskDesc(
                name=f"waiting for {self._app_name} install", parent=parent_task_desc
            )
            td_wait.task_started()  # type: ignore
            args = ["wait"] + list(self._app_installed_indicator)

        else:
            return True
            # ctl = await self._kube.ctl(*args, raise_exception=False)
            # rc = await ctl.returncode
            #
            # return rc == 0

        # TODO: add 'global' timeout
        while True:
            ctl = await self._kube.ctl(*args, raise_exception=False)
            if await ctl.returncode == 0:
                break
            else:
                await anyio.sleep(KubeApp.default_wait_recheck_interval)

        td_wait.task_finished(f"{self._app_name} ready")  # type: ignore

        return True

    async def install(
        self, wait: bool = True, parent_task_desc: Optional[TaskDesc] = None
    ) -> None:

        if self.namespace:
            await self._kube.ensure_namespace(self.namespace)

        return await self.install_kube_app(wait=wait, parent_task_desc=parent_task_desc)

    async def install_kube_app(
        self, wait: bool = True, parent_task_desc: Optional[TaskDesc] = None
    ):

        if self.namespace:
            msg = f"installing manifests for '{self._app_name}' into namespace '{self.namespace}'"
        else:
            msg = f"installing manifests for '{self._app_name}'"

        td_install = FreckworksTaskDesc(
            name=f"installing kube app '{self._app_name}'",
            msg=msg,
            parent=parent_task_desc,
        )
        td_install.task_started()

        await self.ensure_manifests(parent_task_desc=td_install)

        if self.namespace:
            args: List[str] = [
                "apply",
                "-n",
                self.namespace,
                "-f",
                self._manifest_folder,
            ]
        else:
            args = ["apply", "-f", self._manifest_folder]

        await self._kube.ctl(*args)

        if wait:
            await self.kube_app_ready(wait=True, parent_task_desc=td_install)

            # if admin_password:
            #     td_pw = FreckOpsTaskDesc(
            #         name="changing argocd admin password", parent=td_install
            #     )
            #     td_pw.task_started()
            #     await self.change_argo_password(
            #         username="admin", new_password=admin_password
            #     )
            #     td_pw.task_finished()
        td_install.task_finished(msg=f"installed '{self._app_name}' manifests")
