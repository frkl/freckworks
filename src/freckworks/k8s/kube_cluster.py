# -*- coding: utf-8 -*-
from abc import ABCMeta, abstractmethod
from typing import Any, Dict, Mapping, Optional

import anyio
from freckworks.k8s.kube import Kube, log
from frtls.async_helpers import wrap_async_task
from frtls.formats.input_formats import auto_parse_dict_string
from frtls.tasks import TaskDesc
from kubernetes_asyncio.config.kube_config import KUBE_CONFIG_DEFAULT_LOCATION


class KubeCluster(metaclass=ABCMeta):
    def __init__(self):

        self._kube = None

    @abstractmethod
    async def get_kubeconfig_path(self) -> str:
        pass

    @abstractmethod
    async def get_context_name(self) -> str:
        pass

    async def get_cluster_info(self) -> Mapping[str, Mapping]:

        cluster_info: Dict[str, Any] = {}
        cluster_info["context"] = await self.get_context_name()
        cluster_info["kubeconfig_path"] = await self.get_kubeconfig_path()
        cluster_info["kubeconfig"] = await self.get_kubeconfig_content()

        return cluster_info

    async def get_kubeconfig_content(self) -> Mapping[str, Any]:

        kubeconfig_path = await self.get_kubeconfig_path()

        async with await anyio.aopen(kubeconfig_path) as f:
            contents = await f.read()

        dict_content = auto_parse_dict_string(contents, content_type="yaml")
        return dict_content

    async def _init_kube_obj(self):

        if self._kube:
            return

        kube_config_path = await self.get_kubeconfig_path()
        context_name = await self.get_context_name()

        self._kube = await Kube.create_obj(
            kubecfg_path=kube_config_path, context_name=context_name
        )

    async def merge_kubeconfig(
        self,
        target_path: Optional[str] = None,
        set_current_context: bool = False,
        backup_orig_config: bool = True,
    ):

        await self.kube.merge_kubeconfig(
            target_path=target_path,
            set_current_context=set_current_context,
            backup_orig_config=backup_orig_config,
        )

    async def unmerge_kubeconfig(
        self,
        target_path: Optional[str] = None,
        new_current_context: Optional[str] = None,
        backup_orig_config: bool = True,
    ):

        await self.kube.unmerge_kubeconfig(
            target_path=target_path,
            new_current_context=new_current_context,
            backup_orig_config=backup_orig_config,
        )

    @property
    def kube(self) -> Kube:

        result = self.get_kube_obj(raise_exception=True)

        return result  # type: ignore

    def get_kube_obj(self, raise_exception=True) -> Optional[Kube]:

        if self._kube is None:
            try:
                wrap_async_task(self._init_kube_obj)
            except (Exception) as e:
                log.debug(f"Can't create Kube obj: {e}", exc_info=True)
                if raise_exception:
                    raise e
                else:
                    return None

        return self._kube

    async def ensure_cluster_exists(self, parent_task_desc: Optional[TaskDesc] = None):

        if not hasattr(self, "_ensure_cluster_exists"):
            raise Exception(
                f"Cluster management class'{type(self)}' can't create Kubernetes clusters. Make sure the cluster exists before calling this command."
            )

        await self._ensure_cluster_exists(  # type: ignore
            parent_task_desc=parent_task_desc
        )  # type: ignore

    async def remove(self, unmerge_config: bool = False) -> None:

        if unmerge_config:
            target = KUBE_CONFIG_DEFAULT_LOCATION
            await self.kube.unmerge_kubeconfig(target)

        if hasattr(self, "ensure_removed"):
            await self.ensure_removed()  # type: ignore
