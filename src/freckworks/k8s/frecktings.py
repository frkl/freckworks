# -*- coding: utf-8 -*-
import os
from typing import Any, Iterable, Mapping

from bring.system_info import get_current_system_info
from freckworks.core.frecktings import FreckTing
from freckworks.defaults import FRECKWORKS_BIN_FOLDER
from freckworks.k8s.kube import Kube
from freckworks.tasks import FreckworksTaskDesc


class KubeClusterTing(FreckTing):

    _plugin_name = "kube"

    async def resolve_values(
        self, *value_names: str, **requirements
    ) -> Mapping[str, Any]:

        kube_config_path = requirements["kube_config_path"]
        context_name = requirements["kube_context"]
        kubectl_path = requirements["kubectl_binary_path"]

        td = FreckworksTaskDesc(
            name="establishing cluster connection", parent=self._task_desc
        )
        td.task_started()
        kube = await Kube.create_obj(
            kubecfg_path=kube_config_path,
            context_name=context_name,
            kubectl_path=kubectl_path,
        )
        td.task_finished()
        # TODO: test cluster connection
        return {"kube": kube}

    def provides(self) -> Mapping[str, str]:

        return {"kube": "Kube"}

    def requires(self) -> Mapping[str, str]:

        return {
            "kube_config_path": "kube_config_path",
            "kube_context": "string",
            "kubectl_binary_path": "kubectl_binary_path",
        }

    def upstream(self) -> Iterable[Mapping[str, Any]]:

        return [{"freckting_type": "kubectl_binary"}]

    def get_args(self):

        return {
            "kube_config_path": {
                "arg_type": "string",
                "doc": "the path to a kube config file",
                "required": True,
            },
            "kubectl_binary_path": {
                "arg_type": "string",
                "doc": "the path to a kubectl binary",
                "required": False,
            },
        }


class KubeAppTing(FreckTing):

    _plugin_name = "kube_app"

    async def resolve_values(
        self, *value_names: str, **requirements
    ) -> Mapping[str, Any]:

        pass

    def provides(self) -> Mapping[str, str]:

        return {}

    def requires(self) -> Mapping[str, str]:

        return {
            "kube": "Kube",
            "app_name": "string",
            "bring_pkgs": "list",
            "manifest_folder": "string",
            "namespace": "string?",
            "app_query_tokens": "list",
        }

    def upstream(self) -> Iterable[Mapping[str, Any]]:

        return []

    def get_args(self) -> Mapping[str, Mapping]:

        return {
            "kube": {"arg_type": "any", "doc": "a Kube python object"},
            "app_name": {"arg_type": "string", "doc": "the name of the KubeApp"},
            "bring_pkgs": {},
        }


class KubectlBinaryTing(FreckTing):

    _plugin_name = "kubectl_binary"

    def provides(self) -> Mapping[str, str]:

        return {"kubectl_binary_path": "string"}

    def requires(self) -> Mapping[str, str]:

        return {
            "kubectl_path": "kubectl_binary_target",
            "kubectl_version": "kubectl_version",
        }

    def upstream(self) -> Iterable[Mapping[str, Any]]:

        return []

    async def resolve_values(
        self, *value_names: str, **requirements
    ) -> Mapping[str, Any]:

        version = requirements.get("kubectl_version", None)
        vars = dict(get_current_system_info())
        vars["version"] = version

        pkg_name = "kubectl"
        target = requirements.get("kubectl_path", FRECKWORKS_BIN_FOLDER)
        pkg_context = "binaries"

        t = await self._freckworks_project.install(
            pkg_name=pkg_name,
            target=target,
            pkg_context=pkg_context,
            parent_task_desc=self.task_desc,
            **vars
        )

        path = os.path.join(t, "kubectl")

        return {"kubectl_binary_path": path}

    def get_args(self) -> Mapping[str, Mapping]:

        return {
            "kubectl_binary_target": {
                "arg_type": "string",
                "doc": "path to install kubectl binary into",
            },
            "kubectl_version": {
                "arg_type": "string",
                "doc": "the version of the kubectl binary",
                "required": False,
                "default": "1.17.2",
            },
        }
