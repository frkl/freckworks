# -*- coding: utf-8 -*-
# async def create_dev_freckops_obj(k3d: K3d, cluster_name: str, kube: Optional[Kube]=None, **kwargs) -> "BaseProject":
#
#     if kube is None:
#         kube = await ensure_dev_cluster(k3d=k3d, cluster_name=cluster_name)
#
#     from freckops.freckops import BaseProject
#     freckops = BaseProject.create(kube)
#     return freckops
#
#
# async def ensure_dev_cluster(k3d: Optional[K3d]=None, cluster_name: Optional[str]=None, **kwargs) -> Kube:
#
#     app_env = AppEnvironment()
#     if not cluster_name:
#         cluster_name = app_env.app_name
#
#     if k3d is None:
#         k3d = K3d(cluster_name=cluster_name)
#
#     cluster_names = await k3d.get_cluster_names()
#     if cluster_name not in cluster_names:
#         await k3d.create_k3d_cluster(cluster_name=cluster_name)
#
#     td_cfg = FreckOpsTaskDesc(name="retrieving cluster config")
#     td_cfg.task_started()
#     kube: Kube = await k3d.create_kube_obj(cluster_name)
#     td_cfg.task_finished()
#
#     return kube
