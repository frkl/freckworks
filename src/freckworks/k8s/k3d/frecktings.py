# -*- coding: utf-8 -*-
import os
from typing import Any, Iterable, Mapping

from bring.system_info import get_current_system_info
from freckworks.core.frecktings import FreckTing
from freckworks.defaults import FRECKWORKS_BIN_FOLDER
from freckworks.k8s.k3d.k3d import K3d
from freckworks.tasks import FreckworksTaskDesc


class K3dTing(FreckTing):

    _plugin_name = "k3d"

    def provides(self) -> Mapping[str, str]:

        return {
            "kube_config_path": "string",
            "kube_context": "string",
            "kube_cluster": "any",
        }

    def requires(self) -> Mapping[str, str]:

        return {"k3d_binary_path": "k3d_binary_path", "cluster_name": "string"}

    def upstream(self) -> Iterable[Mapping[str, Any]]:

        return [{"freckting_type": "k3d_binary"}]

    async def resolve_values(
        self, *value_names: str, **requirements
    ) -> Mapping[str, Any]:

        k3d_path = requirements["k3d_binary_path"]
        k3d_base_path = os.path.dirname(k3d_path)
        k3d = K3d(
            cluster_name=requirements["cluster_name"], search_path=[k3d_base_path]
        )
        await k3d.ensure_cluster_exists(parent_task_desc=self._task_desc)

        td_kcp = FreckworksTaskDesc(
            name="retrieving kubeconfig", parent=self._task_desc
        )
        td_kcp.task_started()
        kcp = await k3d.get_kubeconfig_path()
        td_kcp.task_finished()

        return {
            "kube_config_path": kcp,
            "kube_context": await k3d.get_context_name(),
            "kube_cluster": k3d,
        }

    def get_args(self) -> Mapping[str, Mapping]:

        return {
            "k3d_binary_path": {"arg_type": "string", "doc": "path to k3d binary"},
            "cluster_name": {"arg_type": "string", "doc": "name of the k3d cluster"},
        }


class K3dBinaryTing(FreckTing):

    _plugin_name = "k3d_binary"

    def provides(self) -> Mapping[str, str]:

        return {"k3d_binary_path": "string"}

    def requires(self) -> Mapping[str, str]:

        return {"k3d_path": "k3d_binary_target", "k3d_version": "k3d_version"}

    def upstream(self) -> Iterable[Mapping[str, Any]]:

        return []

    async def resolve_values(
        self, *value_names: str, **requirements
    ) -> Mapping[str, Any]:

        version = requirements.get("k3d_version", "stable")
        vars = dict(get_current_system_info())
        vars["version"] = version

        pkg_name = "k3d"
        target = requirements.get("k3d_path", FRECKWORKS_BIN_FOLDER)
        pkg_context = "binaries"

        t = await self._freckworks_project.install(
            pkg_name=pkg_name,
            target=target,
            pkg_context=pkg_context,
            parent_task_desc=self.task_desc,
            **vars
        )

        path = os.path.join(t, "k3d")

        return {"k3d_binary_path": path}

    def get_args(self) -> Mapping[str, Mapping]:

        return {
            "k3d_binary_target": {
                "arg_type": "string",
                "doc": "path to install k3d binary into",
            },
            "k3d_version": {
                "arg_type": "string",
                "doc": "the version of the k3d binary",
                "required": False,
                "default": "1.7.0",
            },
        }
