# -*- coding: utf-8 -*-
import logging
import os
from typing import Any, Dict, Iterable, List, Mapping, Optional

import anyio
from freckworks.defaults import FRECKWORKS_PATH
from freckworks.k8s.kube_cluster import KubeCluster
from freckworks.tasks import FreckworksTaskDesc
from frtls.exceptions import FrklException
from frtls.formats.input_formats import auto_parse_dict_string
from frtls.introspection.pkg_env import AppEnvironment
from frtls.networking import find_free_port
from frtls.subprocesses import AsyncSubprocess, SubprocessException
from frtls.tasks import TaskDesc


log = logging.getLogger("freckworks")


class K3d(KubeCluster):
    def __init__(
        self, cluster_name: Optional[str] = None, search_path: Iterable[str] = None
    ):

        app_details = AppEnvironment()
        if cluster_name is None:
            cluster_name = app_details.get_pkg_metadata_value(
                "freckworks_default_cluster_name"
            )

        if search_path is None:
            app_dirs = app_details.get_app_dirs()
            if app_dirs is not None:
                search_path = [
                    os.path.join(app_dirs.user_data_dir, "bin")
                ] + FRECKWORKS_PATH
            else:
                search_path = FRECKWORKS_PATH

        self._search_path = search_path

        self._cluster_name: str = cluster_name
        self._clusters_info: Optional[Dict[str, Dict]] = None

        super().__init__()

    @property
    def default_cluster_name(self) -> str:
        return self._cluster_name

    def _get_cluster_name(self, cluster_name: Optional[str] = None) -> str:

        if cluster_name is None:
            cluster_name = self._cluster_name

        if not cluster_name:
            raise FrklException(msg="No cluster name provided.")

        return cluster_name

    async def get_context_name(self) -> str:

        return self._cluster_name

    async def get_kubeconfig_path(self) -> str:

        return await self.get_k3d_kubeconfig_path()

    async def get_k3d_kubeconfig_path(
        self, cluster_name: Optional[str] = None, check_cluster_running: bool = True
    ) -> str:

        cluster_name = self._get_cluster_name(cluster_name)

        k_path: str = os.path.expanduser(
            f"~/.config/k3d/{cluster_name}/kubeconfig.yaml"
        )
        if not os.path.exists(k_path):

            if check_cluster_running:
                clusters = await self.get_clusters_info(update=True)
                if cluster_name not in clusters.keys():
                    raise Exception(
                        f"K3d cluster with name '{cluster_name}' does not exist."
                    )

            i = 0
            _k_path: Optional[str] = None
            while i < 40:

                i = i + 1
                get_kubeconfig = AsyncSubprocess(
                    "k3d",
                    "get-kubeconfig",
                    "--name",
                    cluster_name,
                    extra_path=self._search_path,
                )
                await get_kubeconfig.run(wait=True, raise_exception=False)

                rc = await get_kubeconfig.returncode
                stdout = await get_kubeconfig.stdout
                stderr = await get_kubeconfig.stderr

                if rc != 0:
                    log.debug(f"No k3d kubeconfig yet: {stdout}/{stderr}")
                    await anyio.sleep(0.4)
                    continue
                else:
                    _k_path = stdout.strip()
                    break

            if _k_path is None:
                raise Exception(
                    f"Can't get kubeconfig for k3d cluster '{cluster_name}'."
                )
            k_path = _k_path

        return k_path

    async def get_cluster_names(self, update: bool = False) -> Iterable[str]:

        info = await self.get_clusters_info(update=update)
        return info.keys()

    async def get_clusters_info(self, update: bool = False) -> Dict[str, Dict]:

        if self._clusters_info is not None and not update:
            return self._clusters_info

        k3d_list = AsyncSubprocess("k3d", "list", extra_path=self._search_path)
        await k3d_list.run(wait=True, raise_exception=False)

        ret_code = await k3d_list.returncode
        stdout = await k3d_list.stdout_lines
        stderr = await k3d_list.stderr

        if (ret_code == 1 and "No clusters found" not in stderr) or ret_code not in [
            0,
            1,
        ]:
            raise SubprocessException(k3d_list)

        self._clusters_info = {}
        if ret_code == 1 and "No clusters found" in stderr:
            return self._clusters_info

        for line in stdout:

            if "NAME" in line and "STATUS" in line:
                continue
            if "+-" in line:
                continue

            tokens = line.split("|")
            if not tokens or len(tokens) == 1:
                continue
            cluster_name = tokens[1].strip()
            image = tokens[2].strip()
            status = tokens[3].strip()
            workers = tokens[4].strip()

            self._clusters_info[cluster_name] = {
                "type": "k3d",
                "status": status,
                "image": image,
                "workers": workers,
            }

        return self._clusters_info

    async def _ensure_cluster_exists(self, parent_task_desc: Optional[TaskDesc] = None):

        await self.ensure_k3d_cluster_exists(parent_task_desc=parent_task_desc)

    async def ensure_k3d_cluster_exists(
        self,
        cluster_name: Optional[str] = None,
        parent_task_desc: Optional[TaskDesc] = None,
    ) -> None:

        cluster_name = self._get_cluster_name(cluster_name)

        cluster_names = await self.get_cluster_names(update=True)
        if cluster_name not in cluster_names:

            await self.create_k3d_cluster(
                cluster_name=cluster_name, parent_task_desc=parent_task_desc
            )

    async def ensure_removed(self):

        cluster_names = await self.get_cluster_names(update=True)

        if self._cluster_name in cluster_names:
            await self.delete_k3d_cluster()

    async def delete_k3d_cluster(self, cluster_name: Optional[str] = None):

        cluster_name = self._get_cluster_name(cluster_name)

        delete_cluster = AsyncSubprocess(
            "k3d", "delete", "--name", cluster_name, extra_path=self._search_path
        )
        await delete_cluster.run(wait=True, raise_exception=True)

    async def get_k3d_cluster_info(
        self, cluster_name: Optional[str] = None
    ) -> Mapping[str, Any]:

        cluster_name = self._get_cluster_name(cluster_name)

        clusters_info = await self.get_clusters_info(update=True)
        cluster = clusters_info.get(cluster_name, None)

        if cluster is None:
            raise FrklException(
                msg=f"Can't retrieve info for cluster '{cluster_name}'.",
                reason="No cluster with that name available.",
            )

        cluster_info = dict(cluster)
        cluster_info["kubeconfig_path"] = await self.get_k3d_kubeconfig_path(
            cluster_name
        )
        cluster_info["kubeconfig"] = await self.get_k3d_kubeconfig(cluster_name)

        return cluster_info

    async def get_k3d_kubeconfig(
        self, cluster_name: Optional[str] = None
    ) -> Mapping[str, Any]:

        kubeconfig_path = await self.get_k3d_kubeconfig_path(cluster_name=cluster_name)

        async with await anyio.aopen(kubeconfig_path) as f:
            contents = await f.read()

        dict_content = auto_parse_dict_string(contents, content_type="yaml")
        return dict_content

    async def create_k3d_cluster(
        self,
        cluster_name: Optional[str] = None,
        port_forwards: Optional[List[str]] = None,
        api_port: Optional[int] = None,
        merge_kubeconfig: bool = False,
        parent_task_desc: Optional[TaskDesc] = None,
    ):
        """Create a k3d cluster.

        - *cluster_name*: the name of the new cluster
        - *port_forwards*: a list of port forward instructions, e.g. '8080:80', or '8080' (which will be converted to '8080:8080')
        - *api_port*: the port for the api to listen on, will be used as starting point if already taken
        - *merge_kubeconfig*: whether to merge the new clusters kubeconfig into the default one
        """

        cluster_name = self._get_cluster_name(cluster_name)

        if api_port is None:
            api_port = 6443

        api_port = find_free_port(api_port)

        task_desc = FreckworksTaskDesc(
            name=f"create cluster '{cluster_name}'",
            msg=f"creating k3d cluster '{cluster_name}' (using api port '{api_port}'",
            parent=parent_task_desc,
        )
        task_desc.task_started()

        args = [
            "create",
            "--api-port",
            str(api_port),
            "--server-arg",
            "--no-deploy=traefik",
            # "--server-arg",
            # "--no-deploy=servicelb",
            "--server-arg",
            "--no-deploy=metrics-server",
            "--name",
            cluster_name,
            "--server-arg",
            "--kube-apiserver-arg=runtime-config=settings.k8s.io/v1alpha1=true",
            "--server-arg",
            "--kube-apiserver-arg=enable-admission-plugins=DefaultStorageClass,DefaultTolerationSeconds,LimitRanger,NamespaceLifecycle,NodeRestriction,PersistentVolumeLabel,ResourceQuota,ServiceAccount,PodPreset",
            # "--server-arg",
            # "--kubelet-arg=containerd=/run/k3s/containerd/containerd.sock"
        ]

        pfs = []
        if port_forwards:
            for pf in port_forwards:
                if ":" in pf:
                    pf_string = pf
                else:
                    pf_string = f"{pf}:{pf}"
                pfs.append("--publish")
                pfs.append(pf_string)

        args.extend(pfs)
        k3d_create = AsyncSubprocess("k3d", *args, extra_path=self._search_path)
        await k3d_create.run(raise_exception=False)

        rc = await k3d_create.returncode
        stdout = await k3d_create.stdout
        stderr = await k3d_create.stderr

        if rc != 0:
            if "already exists" in stderr:
                msg = f"Can't create cluster '{cluster_name}', a cluster with that name already exists."
            else:
                msg = "n/a"
                if stdout:
                    msg = stdout
                if stderr:
                    if not msg:
                        msg = stderr
                    else:
                        msg = f"{msg} / {stderr}"
            exc_msg = f"Cluster creation failed: {msg}"
            task_desc.task_failed(failed_msg=exc_msg)
            raise FrklException(msg=exc_msg)
        else:
            task_desc.task_finished(f"cluster '{cluster_name}' created")
