[![PyPI status](https://img.shields.io/pypi/status/freckworks.svg)](https://pypi.python.org/pypi/freckworks/)
[![PyPI version](https://img.shields.io/pypi/v/freckworks.svg)](https://pypi.python.org/pypi/freckworks/)
[![PyPI pyversions](https://img.shields.io/pypi/pyversions/freckworks.svg)](https://pypi.python.org/pypi/freckworks/)
[![Pipeline status](https://gitlab.com/frkl/freckworks/badges/develop/pipeline.svg)](https://gitlab.com/frkl/freckworks/pipelines)
[![Code style](https://img.shields.io/badge/code%20style-black-000000.svg)](https://github.com/ambv/black)

# freckworks

*Generic devops utility framework*


## Description

Documentation still to be done.

# Development

Assuming you use [pyenv](https://github.com/pyenv/pyenv) and [pyenv-virtualenv](https://github.com/pyenv/pyenv-virtualenv) for development, here's how to setup a 'freckworks' development environment manually:

    pyenv install 3.7.3
    pyenv virtualenv 3.7.3 freckworks
    git clone https://gitlab.com/frkl/freckworks
    cd <freckworks_dir>
    pyenv local freckworks
    pip install -e .[all-dev]
    pre-commit install


## Copyright & license

Please check the [LICENSE](/LICENSE) file in this repository (it's a short license!), also check out the [*freckles* license page](https://freckles.io/license) for more details.

[Parity Public License 6.0.0](https://licensezero.com/licenses/parity)

[Copyright (c) 2019 frkl OÜ](https://frkl.io)
